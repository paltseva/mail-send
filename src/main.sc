require: scripts/function.js
require: patterns.sc

theme: /

    state: start
        q!: * *start
        a: Привет! Напиши письмо и я его отправлю

    state: Hi
        q!: Привет
        a: Вы сказали: {{$parseTree.text}}
    
    state: mailtest
        q!: mailtest
        script:
            $mail.send({
                from: "jeesfreeze@gmail.com",
                hiddenCopy: [],
                to: ["d.mityushin@just-ai.com"],
                subject: "тестовое письмо",
                content: "я шлю почту из сценария, лалала",
                smtpHost: "smtp.gmail.com",
                smtpPort: "465",
                user: "jeesfreeze@gmail.com",
                password: "jxronflvntnftasx"
            });
            $reactions.answer("Отправил почту");
        
    state: mail
        q!: * письмо * 
        a: Из сценария или из скрипта?
        
        state: script
            q: * $script * 
            a: yandex или gmail ? 
            
            state: yandex
                q: * yandex *
                script:
                    sendMailyandex()
            
            state: gmail
                q: * gmail *
                script:
                    sendMailgmail();
                    $mail.send({
                       from: "*****",
                       hiddenCopy: [],
                       to: ["*****"],
                       subject: "тестовое письмо",
                       content: "я шлю почту из сценария, лалала. порт ",
                       smtpHost: "smtp.gmail.com",
                       smtpPort: "465",
                       user: "*****",
                       password: "*****"
                       });
                    $reactions.answer("Отправил почту из гугла");
                
                
        state: fromHere
            q: * $here *
            a: yandex или gmail ? 
            
            state: yandex
                q: * $yandex *
                script:
                    $mail.send({
                        from: "*****",
                        hiddenCopy: [],
                        to: ["*****"],
                        subject: "тестовое письмо из яндекса",
                        content: "я шлю почту из сценария, лалала",
                        smtpHost: "smtp.yandex.ru",
                        smtpPort: "465",
                        user: "*****",
                        password: "*****"
                    });
                    $reactions.answer("Отправил почту");
                    
            state: gmail
                q: * $gmail *
                script:
                    $mail.send({
                        from: "*******",
                        hiddenCopy: [],
                        to: ["*****"],
                        subject: "тестовое письмо из gmail",
                        content: "я шлю почту из сценария, лалала. порт ",
                        smtpHost: "smtp.gmail.com",
                        smtpPort: "465",
                        user: "*****",
                        password: "******"
                    });
                    $reactions.answer("Отправил почту из гугла");
                    
    state: catchAll
        q!: $catchAll
        a: Не понял
            
        
            
    
    